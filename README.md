# MoneyTransferService


## Ingenico programming assignment 


## Features
 - The ability to create an account with initial balance
 - The ability to transfer money from one account to another

### How to run
This Spring Boot-based project is built with Maven and requires a MySQL database. In order to run the application, it is necessary to create a database first.
Read the application.properties for further details.
 
You can use the __spring-boot:run__ command to run the application. The port is set to 5000.

### How to test
Use the command: __mvn test__ to run the tests.

#### 1. Add an account

__Url:__ http://localhost:5000/api/account/

Example json:
```
{
	"firstName": "Jamesson",
	"lastName": "Smith",
	
	"initialBalance": 
		{
			"value": 100.00,
			"currency": "EUR"
		}
		
}
```
To make the __POST__ request:
```
curl -H "Content-Type: application/json" -v -X POST -d '{
    "firstName": "Jamesson",
	"lastName": "Smith",
	"initialBalance": {
	    "value": 100.00,
		"currency": "EUR"
		}
}' http://localhost:5000/api/account
```
#### 2. Transfer money from one account to another
Once the account is created you can use the ibanNumber to make the transfer request.

__Url:__ http://localhost:5000/api/payment/

Example json:

```
{ 
	"orderingAccount": "NL12ADYB5084061512", 
	"beneficiaryAccount": "NL28ADYB0914424850", 
	"amount": { 
		"value":10.00, 
		"currency": "EUR" 
	} 
} 
```

To make the __POST__ request:
```
curl -H "Content-Type: application/json" -v -X POST -d '{ 
	"orderingAccount": "NL12ADYB5084061512", 
	"beneficiaryAccount": "NL28ADYB0914424850", 
	"amount": { 
		"value":10.00, 
		"currency": "EUR" 
	} 
}' http://localhost:5000/api/payment/ 
```