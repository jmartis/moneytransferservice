package com.minibank.transferservice.service;

import com.minibank.transferservice.entity.Account;
import com.minibank.transferservice.entity.AccountBalance;
import com.minibank.transferservice.entity.AccountType;
import com.minibank.transferservice.entity.Customer;
import com.minibank.transferservice.payload.AccountRequest;
import com.minibank.transferservice.payload.AmountRequest;
import com.minibank.transferservice.payload.PaymentInstructionRequest;
import com.minibank.transferservice.payload.PaymentInstructionResponse;
import com.minibank.transferservice.repository.CustomerRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.Optional;

import static junit.framework.TestCase.assertEquals;

@SpringBootTest
@RunWith(SpringRunner.class)
public class PaymentInstructionServiceTest {

    @Autowired
    private AccountService accountService;

    @Autowired
    private PaymentInstructionService paymentInstructionService;

    @Test
    public void testTransferRequestWithInsufficientBalanceShouldFail() {
        String firstName = "ASimple";
        String lastName = "Tester";
        AmountRequest initialBalance =  new AmountRequest(new BigDecimal(10));

        AccountRequest accountRequest = new AccountRequest(firstName, lastName, AccountType.CURRENT_ACCOUNT.toString(), initialBalance);

        Account firstAccount = accountService.createAccount(accountRequest);

        assertEquals(firstName, firstAccount.getCustomer().getFirstName());

        String anotherFirstName = "Second";
        String anotherLastName  = "Tester";
        AmountRequest secondInitialBalance =  new AmountRequest(new BigDecimal(100));

        AccountRequest secondAccountRequest = new AccountRequest(anotherFirstName, anotherLastName, AccountType.CURRENT_ACCOUNT.toString(), secondInitialBalance);

        Account secondAccount = accountService.createAccount(secondAccountRequest);

        assertEquals(anotherFirstName, secondAccount.getCustomer().getFirstName());

        String orderingAccount = firstAccount.getAccountNumber();
        String beneficiaryAccount = secondAccount.getAccountNumber();
        AmountRequest transferAmount = new AmountRequest(new BigDecimal(20));
        PaymentInstructionRequest paymentRequest = new PaymentInstructionRequest(orderingAccount, beneficiaryAccount, transferAmount);

        Optional<PaymentInstructionResponse> response = paymentInstructionService.createPaymentInstruction(paymentRequest);

        Assert.assertFalse(response.isPresent());

    }



}
