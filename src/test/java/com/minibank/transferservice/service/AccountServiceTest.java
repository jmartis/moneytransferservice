package com.minibank.transferservice.service;

import com.minibank.transferservice.payload.AccountRequest;
import com.minibank.transferservice.payload.AmountRequest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;

@SpringBootTest
@RunWith(SpringRunner.class)
public class AccountServiceTest {


    @Autowired
    private AccountService accountService;

    @Test(expected = NumberFormatException.class)
    public void testCreateAccountWithEmptyFieldShouldThrowError() {
        String firstName = "Foo";
        String lastName = "Bar";
        String accountType = "current";
        String amount = "10.A";
        AmountRequest initialBalance = new AmountRequest(amount);
        AccountRequest accountRequest = new AccountRequest(firstName, lastName, accountType, initialBalance );
        accountService.createAccount(accountRequest);

    }

    @Test(expected = DataIntegrityViolationException.class)
    public void testCreateAccountWithSameCredentialsShouldThrowError() {
        AmountRequest initialBalance = new AmountRequest(new BigDecimal(100), "EUR");
        String firstName = "John";
        String lastName = "Doe";
        String accountType = "";
        AccountRequest accountRequest = new AccountRequest(firstName, lastName, accountType, initialBalance);
        accountService.createAccount(accountRequest);
        accountService.createAccount(accountRequest);
    }



}
