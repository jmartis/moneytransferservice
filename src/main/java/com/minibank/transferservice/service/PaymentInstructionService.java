package com.minibank.transferservice.service;

import com.minibank.transferservice.entity.Account;
import com.minibank.transferservice.entity.AccountBalance;
import com.minibank.transferservice.entity.PaymentInstruction;
import com.minibank.transferservice.exception.BankAccountNotFoundException;
import com.minibank.transferservice.payload.AmountRequest;
import com.minibank.transferservice.payload.PaymentInstructionRequest;
import com.minibank.transferservice.payload.PaymentInstructionResponse;
import com.minibank.transferservice.repository.AccountBalanceRepository;
import com.minibank.transferservice.repository.AccountRepository;
import com.minibank.transferservice.repository.PaymentInstructionRepository;
import com.minibank.transferservice.util.IbanUtils;
import com.minibank.transferservice.util.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Optional;

@Service
public class PaymentInstructionService {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private AccountBalanceRepository accountBalanceRepository;

    @Autowired
    private PaymentInstructionRepository paymentInstructionRepository;


    @Transactional(rollbackFor = Exception.class)
    public Optional<PaymentInstructionResponse> createPaymentInstruction(PaymentInstructionRequest request) {

        // validate customer iban from request
        String orderingAcctIbanNumber = IbanUtils.validateIban(request.getOrderingAccount());
        // validate beneficiary iban from request
        String beneficiaryAcctIbanNumber = IbanUtils.validateIban(request.getBeneficiaryAccount());

        // get account of customer
        Account orderingAccount = accountRepository.findAccountByAccountNumber(orderingAcctIbanNumber).orElseThrow(() -> new BankAccountNotFoundException("Bank account", "IBAN", orderingAcctIbanNumber));
        // get account of beneficiary
        Account beneficiaryAccount = accountRepository.findAccountByAccountNumber(beneficiaryAcctIbanNumber).orElseThrow(() -> new BankAccountNotFoundException("Bank account", "IBAN", beneficiaryAcctIbanNumber));

        // get account balance from ordering account
        AccountBalance orderingAcctCurrentBalance = orderingAccount.getAccountBalance();
        AmountRequest transferAmount = request.getAmount();

        // if funds is insufficient, do not make the transaction. return error message
        if (!hasSufficientFunds(orderingAcctCurrentBalance, transferAmount)) {
            return Optional.empty();
        }

        Instant now = Instant.now();
        PaymentInstruction paymentInstruction = new PaymentInstruction(now, orderingAccount, beneficiaryAccount);
        paymentInstruction.setAmount(transferAmount.getValue());

        paymentInstructionRepository.save(paymentInstruction);

        // get account balance from beneficiary account
        AccountBalance beneficiaryAcctCurrentBalance = beneficiaryAccount.getAccountBalance();

        // make the transaction
        // 1. subtract the amount to be sent from the ordering account
        BigDecimal orderingAcctBalance = orderingAcctCurrentBalance.getBalance().subtract(transferAmount.getValue());
        AccountBalance orderingAccountRemainingBalance = new AccountBalance();
        orderingAccountRemainingBalance.setId(orderingAcctCurrentBalance.getId());
        orderingAccountRemainingBalance.setAccount(orderingAccount);
        orderingAccountRemainingBalance.setCurrency(orderingAcctCurrentBalance.getCurrency());
        orderingAccountRemainingBalance.setBalance(orderingAcctBalance);

        accountBalanceRepository.save(orderingAccountRemainingBalance);

        // 2. add the amount to the beneficiary account
        BigDecimal beneficiaryAcctBalance = beneficiaryAcctCurrentBalance.getBalance().add(transferAmount.getValue());
        AccountBalance beneficiaryAcctUpdatedBalance = new AccountBalance();
        beneficiaryAcctUpdatedBalance.setId(beneficiaryAcctCurrentBalance.getId());
        beneficiaryAcctUpdatedBalance.setAccount(beneficiaryAccount);
        beneficiaryAcctUpdatedBalance.setCurrency(transferAmount.getCurrency());
        beneficiaryAcctUpdatedBalance.setBalance(beneficiaryAcctBalance);

        accountBalanceRepository.save(beneficiaryAcctUpdatedBalance);

        Account account  = accountRepository.findAccountById(orderingAccount.getId());
        // return the response payload containing the new balance of the ordering account
        return ModelMapper.mapPaymentToResponse(account, transferAmount.getValue());

    }


    private boolean hasSufficientFunds(AccountBalance balance, AmountRequest transferAmount) {
        return balance.getBalance().subtract(transferAmount.getValue()).compareTo(BigDecimal.ZERO) > 0;
    }



}
