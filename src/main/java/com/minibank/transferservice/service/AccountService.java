package com.minibank.transferservice.service;

import com.minibank.transferservice.entity.Account;
import com.minibank.transferservice.entity.AccountBalance;
import com.minibank.transferservice.entity.AccountType;
import com.minibank.transferservice.entity.Customer;
import com.minibank.transferservice.payload.AccountRequest;
import com.minibank.transferservice.repository.AccountBalanceRepository;
import com.minibank.transferservice.repository.AccountRepository;
import com.minibank.transferservice.repository.CustomerRepository;
import com.minibank.transferservice.util.IbanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AccountService {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    AccountBalanceRepository accountBalanceRepository;

    @Transactional
    public Account createAccount(AccountRequest request) {
        String firstName = request.getFirstName();
        String lastName = request.getLastName();
        Customer customer = new Customer(firstName, lastName);
        Customer createdCustomer = customerRepository.save(customer);

        Account account = new Account();
        account.setCustomer(createdCustomer);
        account.setAccountType(AccountType.CURRENT_ACCOUNT);

        account.setAccountNumber(IbanUtils.createIbanNumber());

        Account createdAccount = accountRepository.save(account);

        AccountBalance accountBalance = new AccountBalance();
        accountBalance.setAccount(createdAccount);
        accountBalance.setBalance(request.getInitialBalance().getValue());
        accountBalance.setCurrency(request.getInitialBalance().getCurrency());

        accountBalanceRepository.save(accountBalance);

        return createdAccount;
    }
}
