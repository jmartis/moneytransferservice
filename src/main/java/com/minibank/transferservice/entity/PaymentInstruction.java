package com.minibank.transferservice.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.Instant;

@Entity
@Table(name = "payment_instruction", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"transaction_date", "ordering_acc_id", "beneficiary_acc_id"}
        )
})
@Getter
@Setter
public class PaymentInstruction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "transaction_date", nullable = false, updatable = false)
    private Instant transactionDate;

    @Column(name = "amount", nullable = false)
    private BigDecimal amount;

    @ManyToOne(optional = false)
    @JoinColumn(name = "ordering_acc_id", nullable = false)
    private Account orderingAccount;

    @ManyToOne(optional = false)
    @JoinColumn(name = "beneficiary_acc_id", nullable = false)
    private Account beneficiaryAccount;


    private PaymentInstruction() {
    }

    public PaymentInstruction(Instant transactionDate, Account orderingAccount, Account beneficiaryAccount) {
        this.transactionDate = transactionDate;
        this.orderingAccount = orderingAccount;
        this.beneficiaryAccount = beneficiaryAccount;
    }


}
