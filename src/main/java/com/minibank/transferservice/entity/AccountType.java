package com.minibank.transferservice.entity;

public enum AccountType {
    CURRENT_ACCOUNT,
    SAVINGS_ACCOUNT
}
