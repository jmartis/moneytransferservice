package com.minibank.transferservice.repository;

import com.minibank.transferservice.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface AccountRepository extends JpaRepository<Account, Long> {

    @Query("SELECT a FROM Account a WHERE a.id =:id")
    Account findAccountById(@Param("id") Long id);

    Optional<Account> findAccountByAccountNumber(String ibanNumber);
}
