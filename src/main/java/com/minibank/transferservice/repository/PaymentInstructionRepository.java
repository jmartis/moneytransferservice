package com.minibank.transferservice.repository;

import com.minibank.transferservice.entity.PaymentInstruction;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PaymentInstructionRepository extends JpaRepository<PaymentInstruction, Long> {
}
