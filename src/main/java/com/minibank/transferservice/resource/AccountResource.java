package com.minibank.transferservice.resource;

import com.minibank.transferservice.entity.Account;
import com.minibank.transferservice.payload.AccountRequest;
import com.minibank.transferservice.payload.AccountResponse;
import com.minibank.transferservice.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping("api/account")
public class AccountResource {

    @Autowired
    private AccountService service;

    @PostMapping
    public ResponseEntity<?> createAccount(@Valid @RequestBody AccountRequest accountRequest) {
        Account account = service.createAccount(accountRequest);


        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{accountId}")
                .buildAndExpand(account.getId()).toUri();


        return ResponseEntity.created(location).body(new AccountResponse(true, "Account created successfully.", account.getAccountNumber()));
    }

}
