package com.minibank.transferservice.resource;

import com.minibank.transferservice.exception.PaymentInstructionException;
import com.minibank.transferservice.payload.PaymentInstructionRequest;
import com.minibank.transferservice.payload.PaymentInstructionResponse;
import com.minibank.transferservice.service.PaymentInstructionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.Optional;

@RestController
@RequestMapping("api/payment")
public class PaymentInstructionResource {

    @Autowired
    private PaymentInstructionService service;

    @PostMapping
    public ResponseEntity<?> createPaymentInstruction(@Valid @RequestBody PaymentInstructionRequest paymentRequest) {
        System.out.println(paymentRequest.getAmount().getCurrency());
        Optional<PaymentInstructionResponse> paymentInstruction  = service.createPaymentInstruction(paymentRequest);

        if (!paymentInstruction.isPresent()) {
            throw new PaymentInstructionException("Insufficient funds.");
        }

        PaymentInstructionResponse response = paymentInstruction.get();

        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{accountId}")
                .buildAndExpand(response.getAccountId()).toUri();

        return ResponseEntity.created(location).body(response);

    }



}
