package com.minibank.transferservice.payload;

import org.javamoney.moneta.Money;
import org.javamoney.moneta.RoundedMoney;

import javax.money.MonetaryAmount;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.concurrent.atomic.AtomicLong;

public class AmountRequest {

    public static final String DEFAULT_CURRENCY_CODE = "EUR";

    @NotNull
    @Digits(integer = 6, fraction = 2)
    private BigDecimal value;

    private String currency;

    private AmountRequest() {
    }

    public AmountRequest(String value) {
        this(new BigDecimal(value));
    }

    public AmountRequest(BigDecimal amount) {
        this(amount, DEFAULT_CURRENCY_CODE);
    }

    public AmountRequest(BigDecimal value, String currency) {
        this(Money.of(value, currency));
    }

    public AmountRequest(MonetaryAmount money) {
        this.value = getBigDecimal(money.getNumber());
        this.currency = money.getCurrency().getCurrencyCode();
    }

    public BigDecimal getValue() {
        return getBigDecimal(getMoney().getNumber());
    }

    public String getCurrency() {
        return getMoney().getCurrency().getCurrencyCode();
    }

    private RoundedMoney getMoney() {
        return RoundedMoney.of(value, currency);
    }

    private static BigDecimal getBigDecimal(Number number) {
        if (number instanceof BigDecimal) {
            return (BigDecimal) number;
        }
        if (number instanceof Long || number instanceof Integer
                || number instanceof Byte || number instanceof AtomicLong) {
            return BigDecimal.valueOf(number.longValue());
        }
        if (number instanceof Float || number instanceof Double) {
            return new BigDecimal(number.toString());
        }

        return BigDecimal.valueOf(number.doubleValue());
    }


}
