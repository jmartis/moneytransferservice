package com.minibank.transferservice.payload;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class AccountResponse {

    private Boolean success;
    private String message;
    private String ibanNumber;

}
