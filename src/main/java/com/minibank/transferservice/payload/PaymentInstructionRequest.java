package com.minibank.transferservice.payload;


import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class PaymentInstructionRequest {

    @NotNull
    @NotBlank
    private String orderingAccount;
    @NotNull
    @NotBlank
    private String beneficiaryAccount;

    private AmountRequest amount;

    private PaymentInstructionRequest() {}

    public PaymentInstructionRequest(String orderingAccount, String beneficiaryAccount, AmountRequest amount) {
        this.orderingAccount = orderingAccount;
        this.beneficiaryAccount = beneficiaryAccount;
        this.amount = amount;
    }


}
