package com.minibank.transferservice.payload;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter @Setter
public class AccountRequest {

    @NotBlank
    @Size(max=25)
    private String firstName;

    @NotBlank
    @Size(max = 25)
    private String lastName;

    private String accountType;

    @NotNull
    private AmountRequest initialBalance;

    private AccountRequest() {}

    public AccountRequest(String firstName, String lastName, String accountType, AmountRequest initialBalance) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.accountType = accountType;
        this.initialBalance = initialBalance;
    }


}
