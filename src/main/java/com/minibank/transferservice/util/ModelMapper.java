package com.minibank.transferservice.util;

import com.minibank.transferservice.entity.Account;
import com.minibank.transferservice.payload.PaymentInstructionResponse;

import java.math.BigDecimal;
import java.util.Optional;

public class ModelMapper {

    public static Optional<PaymentInstructionResponse> mapPaymentToResponse(Account account, BigDecimal transferredAmount) {
        PaymentInstructionResponse paymentInstructionResponse = new PaymentInstructionResponse();
        paymentInstructionResponse.setAccountNumber(account.getAccountNumber());
        paymentInstructionResponse.setAccountId(account.getId());
        paymentInstructionResponse.setAccountBalance(account.getAccountBalance().getBalance());
        paymentInstructionResponse.setTransferredAmount(transferredAmount);

        return Optional.of(paymentInstructionResponse);
    }
}
