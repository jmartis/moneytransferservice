package com.minibank.transferservice.util;

import org.iban4j.*;

public class IbanUtils {

    public static String createIbanNumber() {
        Iban iban = new Iban.Builder()
                .countryCode(CountryCode.NL)
                .bankCode("ADYB")
                .buildRandom();

        return iban.toString();
    }

    public static String validateIban(String iban) {
        String ibanNumber = iban.trim();
        try {
            IbanUtil.validate(ibanNumber);
        } catch (IbanFormatException | InvalidCheckDigitException | UnsupportedCountryException e) {
            e.printStackTrace();
        }

        return ibanNumber;
    }
}
