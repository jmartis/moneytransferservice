package com.minibank.transferservice.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
public class PaymentInstructionException extends RuntimeException {

    public PaymentInstructionException(String message) {
        super(message);
    }

    public PaymentInstructionException(String message, Throwable cause) {
        super(message, cause);
    }
}
